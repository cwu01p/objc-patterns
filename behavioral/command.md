# Command

Encapsulate a request as an object, thereby letting you parameterize clients with different requests, queue or log requests, and support undoable operations.

## Implementation

```
// Command.h
@protocol Command <NSObject>

- (id)execute;

@end
```

```
// Plumber.h
@interface Plumber : NSObject

- (BOOL)fixLeak;

@end
```

```
// Plumber.m
#import "Plumber.h"

@implementation Plumber

- (BOOL)fixLeak {
    NSLog(@"Fixing leak...");
    NSLog(@"Fixed leak.");
    return YES;
}

@end
```

```
// PlumberFixLeakCommand.h
#import "Command.h"
#import "Plumber.h"

@interface PlumberFixLeakCommand : NSObject <Command>

- (instancetype)initWithPlumber:(Plumber *)plumber;

@end
```

```
// PlumberFixLeakCommand.m

@interface PlumberFixLeakCommand()
@property (strong, nonatomic) Plumber *plumber;
@end

@implementation PlumberFixLeakCommand

- (instancetype)init {
    return nil;
}

- (instancetype)initWithPlumber:(Plumber *)plumber {
    self = [super init];
    self.plumber = plumber;
    return self;
}

- (id)execute {
    return @([self.plumber fixLeak]);
}

@end
```

## Usage

```
Plumber *plumber = [[Plumber alloc] init];
PlumberFixLeakCommand *command = [[PlumberFixLeakCommand alloc] initWithPlumber:plumber];
id fixedLeak = [command execute];
if ([fixedLeak boolValue]) {
    // FIXED
} else {
    // Handle error
}
```

## Rules of Thumb

* Command addresses how you can decouple senders and receivers, but with different trade-offs. Command normally specifies a sender-receiver connection with a subclass.
* Chain of Responsibility can use Command to represent requests as objects.
* Command and Memento act as magic tokens to be passed around and invoked at a later time. In Command, the token represents as a request; in Memento, it represents the internal state of an object at a particular time. Polymorphism is important to Command, but not to Memento because its interface is so narrow that a memento can only be passed as a value.
* Command can use Memento to maintain the state required for an undo operation.
* Two important aspects of the Command pattern: interface separation (the invoker is isolated from the receiver), time separation (stores a ready-to-go processing request that's to be stated later).