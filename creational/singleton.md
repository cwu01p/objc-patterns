# Singleton Pattern

Singleton creational design pattern restricts the instantiation of a type to a single object.

## Implementation

```
@interface Singleton : NSObject

+ (instancetype)sharedInstance;
- (void)someMethod;

@end
    
@implementation Singleton

+ (instancetype)sharedInstance {
    static id Instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Instance = [[self alloc] init];
    });
    return Instance;   
}

- (void)someMethod {
    NSLog(@"-[Singleton someMethod]");
}

@end
```

## Usage

```
Singleton singleton = [Singleton sharedInstance];
[singleton someMethod];
```

## Rules of thumb

* Singleton pattern represents a global state and most of the time reduces testability.