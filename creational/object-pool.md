# Object Pool

The object pool creational design pattern is used to prepare and keep multiple instances according to the demand expectation.

## Implementation

```
// ObjectPool.h
#import <Foundation/Foundation.h>

@interface ObjectPool<ObjectType> : NSObject

typedef ObjectType (^InitBlock)(void);

@property (nonatomic, readonly) NSArray<ObjectType> *allObjects;

- (instancetype)initWithCount:(NSUInteger)count initBlock:(InitBlock)initBlock;
- (ObjectType)borrowObject;
- (void)returnObjectToPool:(ObjectType)anObject;

@end
```

```
// ObjectPool.m

#import "ObjectPool.h"

@interface ObjectPool()
@property (nonatomic, copy) InitBlock initBlock;
@property (strong, nonatomic) NSMutableArray *pool;
@end

@implementation ObjectPool

- (instancetype)initWithCount:(NSUInteger)count initBlock:(InitBlock)initBlock {
    self = [self init];
    
    self.initBlock = initBlock;
    
    self.pool = [NSMutableArray arrayWithCapacity:count];
    for (NSUInteger i = 0; i < count; i++) {
        id anObject = self.initBlock();
        [self.pool addObject:anObject];
    }
    
    return self;
}

- (id)borrowObject {
    @synchronized(self) {
        id borrowedObject = [self.pool lastObject];
        [self.pool removeLastObject];
        
        if (borrowedObject == nil) {
            borrowedObject = self.initBlock();
        }
        
        return borrowedObject;
    }
}

- (void)returnObjectToPool:(id)anObject {
    [self.pool addObject:anObject];
}

@end
```



## Usage

```
ObjectPool<NSObject *> *pool = [[ObjectPool alloc] initWithCount:5 initBlock:^id{
    return [[NSObject alloc] init];
}];

NSObject *borrowedObject = [pool borrowObject];

// ... some logic

[pool returnObjectToPool:borrowedObject];
```

## Rules of thumb

* Object pool pattern is useful in cases where object initialization is more expensive than the object maintenance.
* If there are spikes in demand as opposed to a steady demand, the maintenance overhead might overweight the benefits of an object pool.
* It has positive effects on performance due to objects being initialized beforehand.