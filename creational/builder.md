# Builder

The intent of the Builder design pattern is to separate the construction of a complex object from its representation. By doing so the same construction process can create different representations.

## Implementation

```
// Actor.h
@interface Actor : NSObject

@property (copy, nonatomic) NSString *firstName;
@property (copy, nonatomic) NSString *lastName;
@property (nonatomic, readonly) NSString *fullName;

@end
```

```
// Actor+Builder.h

#import "ActorBuilder.h"

@interface Actor (Builder)

- (instancetype)initWithBuilder:(ActorBuilder *)builder;

@end
```

```
// Actor+Builder.m
#import "Actor+Builder.h"

@implementation Actor (Builder)

- (instancetype)initWithBuilder:(ActorBuilder *)builder {
    self = [self init];
    self.firstName = builder.firstName;
    self.lastName = builder.lastName;
    return self;
}

@end
```

```
// ActorBuilder.h

@class Actor;

@interface ActorBuilder : NSObject

@property (copy, nonatomic) NSString *firstName;
@property (copy, nonatomic) NSString *lastName;

+ (Actor *)makeActorWithBuilder:(void(^)(ActorBuilder *builder))builder;

@end
```

```
// ActorBuilder.m

#import "ActorBuilder.h"
#import "Actor+Builder.h"

@implementation ActorBuilder

+ (Actor *)makeActorWithBuilder:(void (^)(ActorBuilder *))builder {
    ActorBuilder *actorBuilder = [ActorBuilder new];
    builder(actorBuilder);
    return [[Actor alloc] initWithBuilder:actorBuilder];
}

@end
```

## Usage

```
Actor *actor = [ActorBuilder makeActorWithBuilder:^(ActorBuilder *builder) {
	builder.firstName = @"First";
    builder.lastName = @"Last";
}];
```

## Rules of thumb

* Use only if dependency injection is less of a need.