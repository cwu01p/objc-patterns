# Class Factory Method

Class factory methods are implemented by a class as a convenience for clients. They combine allocation and initialization in one step and return the created object.

## Implementation

### Types

```
@interface Foo : NSObject

@property (copy, nonatomic) NSString *aString;

+ (instancetype)foo;
+ (instancetype)fooWithString:(NSString *)string;

@end

@implementation Foo

+ (instancetype)foo {
    return [[[self class] alloc] init];
}

+ (instancetype)fooWithString:(NSString *)string {
    typeof (self) foo = [self foo];
    foo.aString = string;
    return foo;
}

@end
```

## Usage

```
Foo *foo = [Foo foo];

Foo *anotherFoo = [Foo fooWithString:@"STRING"];
```

## Rules of thumb

* Factory methods can be more than a simple convenience. They can not only combine allocation and initialization, but the allocation can inform the initialization.
* Another purpose for a class factory method is to ensure that a certain class vends a singleton instance.