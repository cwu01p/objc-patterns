# Abstract Factory

The abstract factory pattern provides a way to encapsulate a group of individual factories that have a common theme without specifying their concrete classes.

## Implementation

```
// CharacterFactory.h

typedef NS_ENUM(NSInteger, CharacterType) {
    CharacterTypeWarrior,
    CharacterTypePriest
    // the list goes on
}

@interface CharacterFactory : NSObject

+ (nullable instancetype)characterFactoryOfType:(CharacterType)characterType;
- (Character *)createCharacter;

@end
```

```
// CharacterFactory.m
#import "CharacterFactory.h"
#import "WarriorFactory.h"
#import "PriestFactory.h"

@implementation CharacterFactory

+ (instancetype)characterFactoryOfType:(CharacterType)characterType {
    switch (characterType) {
        case CharacterTypeWarrior:
        	return [[WarriorFactory alloc] init];
        case CharacterTypePriest:
        	return [[PriestFactory alloc] init];
    }
    
    return nil;
}

- (Character *)createCharacter {
	NSString *exceptionName = NSInternalInconsistencyException;
	NSString *exceptionReason = [NSString stringWithFormat:@"-[%@ createCharacter] was not implemented.", [self class]];
    @throw [NSException exceptionWithName:exceptionName reason:exceptionReason userInfo:nil];
}

@end
```

```
// WarriorFactory.m
#import "WarriorFactory.h"

@implementation WarriorFactory 

- (Character *)createCharacter {
    return [[Warrior alloc] init];
}

@end
```

## Usage

```
CharacterFactory *factory = [CharacterFactory characterFactoryOfType:CharacterTypeWarrior];

// You may have to cast it to Warrior class when necessary but it should work with polymorphism.
Character *warrior = [factory createCharacter];
```

## Rules of thumb

* Abstract factory is a very central design pattern for **Dependency Injection**.
* Abstract factory is useful when an external factor decides which objects need to be created and this **flexibility** is required.
* Factories are not included in your business code, thus having lots of `case` s to your `switch` clause is acceptable and doesn't really break **Open-Closed Principle**.