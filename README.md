## Creational Patterns

| Pattern                                                      | Description                                                  | Status |
| ------------------------------------------------------------ | ------------------------------------------------------------ | :----- |
| [Abstract Factory](https://bitbucket.org/cwu01p/objc-patterns/src/master/creational/abstract-factory.md) | Provides an interface for creating families of related objects. | ✔      |
| [Builder](https://bitbucket.org/cwu01p/objc-patterns/src/master/creational/abstract-factory.md?at=master&fileviewer=file-view-default) | Builds a complex object using simple objects.                | ✔      |
| [Class Factory Method](https://bitbucket.org/cwu01p/objc-patterns/src/master/creational/class-factory-method.md?at=master) | Defers instantiation of an object to a specialized function of creating instances. | ✔      |
| [Object Pool](https://bitbucket.org/cwu01p/objc-patterns/src/master/creational/object-pool.md?at=master&fileviewer=file-view-default) | Instantiates and maintains a group of objects instances of the same type. | ✔      |
| [Singleton](https://bitbucket.org/cwu01p/objc-patterns/src/master/creational/singleton.md?at=master&fileviewer=file-view-default) | Restricts instantiation of a type to one object.             | ✔      |

## Structural Patterns

| Pattern   | Description                                                  | Status |
| --------- | ------------------------------------------------------------ | ------ |
| Bridge    | Decouples an interface from its implementation so that the two can vary independently. | ✘      |
| Composite | Encapsulates and provides access to a number of different objects. | ✘      |
| Decorator | Adds behavior to an object, statically or dynamically.       | ✘      |
| Facade    | Uses one type as an API to a number of others                | ✘      |
| Flyweight | Reuses existing instances of objects with similar/identical state to minimize resource usage. | ✘      |
| Proxy     | Provides a surrogate for an object to control it's actions.  | ✘      |

## Behavioral Patterns

| Pattern                                                      | Description                                                  | Status |
| ------------------------------------------------------------ | ------------------------------------------------------------ | :----- |
| Chain of Responsibility                                      | Avoids coupling a sender to its receiver by giving more than one object a chance to handle the request. | ✘      |
| [Command](https://bitbucket.org/cwu01p/objc-patterns/src/master/behavioral/command.md?at=master&fileviewer=file-view-default) | Bundles a command and arguments to call later.               | ✔      |
| Mediator                                                     | Connects objects and acts as a proxy.                        | ✘      |
| Memento                                                      | Generate an opaque token that can be used to go back to a previous state. | ✘      |
| Observer                                                     | Provide a callback for notification of events/changes to data. | ✘      |
| Registry                                                     | Keep track of all subclasses of a given class.               | ✘      |
| State                                                        | Encapsulates varying behavior for the same object based on its internal state. | ✘      |
| Strategy                                                     | Enables an algorithm's behavior to be selected at runtime.   | ✘      |
| Template                                                     | Defines a skeleton class which defers some methods to subclasses. | ✘      |
| Visitor                                                      | Separates an algorithm from an object on which it operates.  | ✘      |

## Synchronization Patterns

| Pattern            | Description                                                  | Status |
| ------------------ | ------------------------------------------------------------ | :----- |
| Condition Variable | Provides a mechanism for threads to temporarily give up access in order to wait for some condition. | ✘      |
| Lock/Mutex         | Enforces mutual exclusion limit on a resource to gain exclusion access. | ✘      |
| Monitor            | Combination of mutex and condition variable patterns.        | ✘      |
| Read-Write Lock    | Allows parallel read access, but only exclusive access on write operations to a resource. | ✘      |
| Semaphore          | Allows controlling access to a common resource.              | ✘      |

## Concurrency Patterns

| Pattern             | Description                                                  | Status |
| ------------------- | ------------------------------------------------------------ | :----- |
| N-Barrier           | Prevents a process from proceeding until all N processes reach to the barrier. | ✘      |
| Bounded Parallelism | Completes large number of independent tasks with resource limits. | ✘      |
| Broadcast           | Transfers a message to all recipients simultaneously.        | ✘      |
| Coroutines          | Subroutines that allow suspending and resuming execution at certain locations. | ✘      |
| Generators          | Yeilds a sequence of values one at a time.                   | ✘      |
| Reactor             | Demultiplexes service requests delivered concurrently to a service handler and dispatches them synchronously to the associated request handlers. | ✘      |
| Parallelism         | Completes large number of independent tasks.                 | ✘      |
| Producer Consumer   | Separates tasks from task executions.                        | ✘      |

## Messaging Patterns

| Pattern            | Description                                                  | Status |
| ------------------ | ------------------------------------------------------------ | :----- |
| Fan-In             | Funnel tasks to work sink (e.g. server.)                     | ✘      |
| Fan-Out            | Distributes tasks among workers (e.g. producer.)             | ✘      |
| Futures & Promises | Acts as a place-holder of a result that is initially unknown for synchronization. | ✘      |
| Publish/Subscribe  | Passes information to a collection of recipients who subscribed to a topic. | ✘      |
| Push & Pull        | Distributes messages to multiple workers, arranged in a pipeline. | ✘      |

## Stability Patterns

| Pattern          | Description                                                  | Status |
| ---------------- | ------------------------------------------------------------ | :----- |
| Bulkheads        | Enforces a principle of failure (i.e. prevents cascading failures.) | ✘      |
| Circuit-Breaking | Stops the flow of the requests when requests are likely to fail. | ✘      |
| Deadline         | Allows clients to stop waiting for a response once the probability of response becomes low (e.g. after waiting 10 seconds for a page refresh.) | ✘      |
| Fail-Fast        | Checks the availability of required resources at the start of a request and fails if the requirements are not satisfied. | ✘      |
| Handshaking      | Asks a component if it can take any more load, it if can't, the request is declined. | ✘      |
| Steady-State     | For every service that accumulates a resource, some other service must recycle that resource. | ✘      |

## Profiling Patterns

| Pattern          | Description                              | Status |
| ---------------- | :--------------------------------------- | :----- |
| Timing Functions | Wraps a function and logs the execution. | ✘      |

## Idioms

| Pattern            | Description                                                  | Status |
| ------------------ | ------------------------------------------------------------ | ------ |
| Functional Options | Allows creating clean APIs with sane defaults and idiomatic overrides. | ✘      |

## Anti-Patterns

| Pattern            | Description                                                  | Status |
| :----------------- | :----------------------------------------------------------- | :----- |
| Cascading Failures | A failure in a system of interconnected parts in which the failure of a part causes a domino effect. | ✘      |

